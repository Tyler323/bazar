@extends('adminlte::page')

@section('title', 'Create commodity')

@section('content_header')
    <h1>Create commodity</h1>
@stop

@section('css')

@stop
@section('js')

    <script>
        $(function () {
            // Summernote
            $('#summernote').summernote()

        })
    </script>

@stop

@section('content')
    <div class="card card-primary">


        <form action="{{ route('commodity.store') }}" method="post" >
            @csrf

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="card-body">
                <div class="form-group">
                    <label for="nameField">Name</label>
                    <input name="name" class="form-control" id="nameField" placeholder="Enter name">
                </div>
                <div class="form-group">
                    <label for="summernote">Description</label>

                    <textarea name="description" id="summernote"></textarea>

                </div>

                <div class="form-group">
                    <label for="priceField">Price</label>
                    <input name="price" class="form-control" id="priceField" placeholder="Enter price">
                </div>

                <div class="form-check">
                    <input name="active" type="checkbox" class="form-check-input" id="activeCheck" checked>
                    <label class="form-check-label" for="activeCheck">Active</label>
                </div>
            </div>

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
@stop

@extends('adminlte::page')

@section('title', 'Commodites')

@section('content_header')
    <h1>Commodites</h1>
@stop

@section('content')

    <div class="card">
        <div class="card-header">
            <h3 class="card-title">List of all commodities</h3>

            <a href="{{ route('commodity.create') }}" class="btn btn-success float-right">Add commodity</a>
        </div>

        @if($errors->any())
            @foreach ($errors->all() as $error)
                <div>{{ $error }}</div>
            @endforeach
        @endif

        <div class="card-body">
            <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Descripotion</th>
                    <th>Price</th>
                    <th>Active</th>
                    <th>Created at</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($commodities as $comodity)
                    <tr>
                        <td>{{ $comodity->name }}</td>
                        <td>{{ $comodity->getShortDesc() }}</td>
                        <td>{{ $comodity->price }}</td>
                        <td>@if($comodity->active) Yes @else No @endif</td>
                        <td>{{ $comodity->created_at }}</td>
                        <td>X</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop

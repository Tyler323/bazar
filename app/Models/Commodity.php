<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Commodity extends Model
{
    use HasFactory;

    protected $guarded = ['files'];

    /**
     * @return string
     */
    public function getShortDesc(): string
    {
        return Str::of(strip_tags($this->description))->words(3, ' ... ');
    }
}

<?php

namespace App\Repositories;

use App\Http\Requests\CommodityRequest;
use App\Models\Commodity;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;

class CommodityRepository {

    /**
     * @param int $id
     *
     * @return Commodity
     */
    public function findById(int $id): Commodity
    {
        return Commodity::find($id)->where('active');
    }

    /**
     * @return Collection
     */
    public function allCommodities(): Collection
    {
        return Commodity::all();
    }

    /**
     * @param CommodityRequest $commodityRequest
     *
     * @return Commodity
     */
    public function storeCommodity(CommodityRequest $commodityRequest): Commodity
    {
        $commodityData = $commodityRequest->all();
        $commodityData['user_id'] = Auth::id();
        $commodityData['active'] = !empty($commodityData['active']) && $commodityData['active'] === 'on';

        return Commodity::create($commodityData);
    }

    /**
     * @param int $id
     *
     * @return mixed
     */
    public function activate(int $id)
    {
        $commodity = Commodity::find($id);
        if(!empty($commodity)) {
            return false;
        }

        $commodity->active = true;

        return $commodity->save();
    }

    /**
     * @param int $id
     *
     * @return mixed
     */
    public function deactivate(int $id)
    {
        $commodity = Commodity::find($id);
        if(!empty($commodity)) {
            return false;
        }

        $commodity->active = false;

        return $commodity->save();
    }

}

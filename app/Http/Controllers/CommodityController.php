<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommodityRequest;
use App\Repositories\CommodityRepository;

class CommodityController extends Controller
{

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $commodities = app(CommodityRepository::class)->allCommodities();

        return view('commodity.index', compact('commodities'));
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('commodity.create');
    }


    /**
     * @param CommodityRequest $commodityRequest
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CommodityRequest $commodityRequest)
    {
        app(CommodityRepository::class)->storeCommodity($commodityRequest);
        return redirect()->route('commodity.index');
    }

}
